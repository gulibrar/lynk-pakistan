from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_designation_bp= Blueprint("system_designation", __name__)

##################################### designation
@system_designation_bp.route('/designation_api/api_get_all_designation', methods=['POST'])
def api_get_all_designation():
    result = 0
    data = ""

    dataSet = designation.select()
    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_designation_bp.route('/designation_api/api_get_single_designation', methods=['POST'])
def api_get_single_designation():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    designation_id = request_json.get('designation_id')

    # post response from API

    dataSet = designation.select().where(designation.designation_id == str(designation_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_designation_bp.route('/designation_api/api_add_new_designation', methods=['POST'])
def api_add_new_designation():
    json_string = ""
    result = ""

    request_json = request.get_json()

    designation_name= request_json.get('designation_name')

    view_status = request_json.get('view_status')

    try:
        query = designation.insert(designation_name=designation_name,view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string


@system_designation_bp.route('/designation_api/api_update_designation', methods=['POST'])
def api_update_designation():
    result = 0
    data = ""

    request_json = request.get_json()

    designation_name= request_json.get('designation_name')
    view_status = request_json.get('view_status')
    designation_id = request_json.get('view_status')

    try:
        query = designation.update(designation_name=designation_name,view_status=view_status).where(designation.designation_id == designation_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_designation_bp.route('/designation_api/api_update_status_designation', methods=['POST'])
def api_update_status_designation():
    result = 0
    request_json = request.get_json()

    designation_id = request_json.get('designation_id')
    view_status = request_json.get('view_status')

    try:
        query = designation.update(view_status=view_status).where(designation.designation_id == designation_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string