from flask import Flask

from .designation_api import system_designation_bp


app = Flask(__name__)

app.register_blueprint(system_designation_bp)