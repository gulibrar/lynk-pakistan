from flask import Flask

from .admin_api import system_admin_api_bp


app = Flask(__name__)

app.register_blueprint(system_admin_api_bp)
