from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_service_type_bp= Blueprint("system_service_type", __name__)

##################################  service type

@system_service_type_bp.route('/service_type_api/api_get_all_service_type',methods=['POST'])
def api_get_all_service_type():

    result=0
    data=""

    dataSet = service_type.select()
    temp_data = list(dataSet.dicts())
    print(temp_data)
    row_num=len(temp_data)
    if row_num==0:
        result=0
        data=""
    else:
        result = 1
        data=temp_data


    json_string = json.dumps({"result": result,"data":data})

    return json_string

@system_service_type_bp.route('/service_type_api/api_get_single_service_type',methods=['POST'])
def api_get_single_service_type():

    result=0
    data=""

    # post response from API
    request_json = request.get_json()
    service_type_id = request_json.get('service_type_id')

    # post response from API

    dataSet = service_type.select().where(service_type.service_type_id == str(service_type_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result,"data":data})
    return json_string

@system_service_type_bp.route('/service_type_api/api_add_service_type',methods=['POST'])
def api_add_service_type():
    json_string = ""
    result = ""

    request_json = request.get_json()

    service_name = request_json.get('service_name')
    service_name_code=request_json.get('service_name_code')
    service_description=request_json.get('service_description')
    service_days=request_json.get('service_days')
    transportation_mode = request_json.get('transportation_mode')
    view_status=request_json.get('view_status')



    try:
        query = service_type.insert(service_name=service_name, service_name_code=service_name_code, service_description=service_description,
                               service_days=service_days,
                                       transportation_mode=transportation_mode,view_status=view_status)


        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})
    return json_string

@system_service_type_bp.route('/service_type_api/api_update_service_type', methods=['POST'])
def api_update_service_type():
    result = 0
    data = ""

    request_json = request.get_json()

    service_name = request_json.get('service_name')
    service_name_code = request_json.get('service_name_code')
    service_description = request_json.get('service_description')
    service_days = request_json.get('service_days')
    transportation_mode = request_json.get('transportation_mode')
    view_status = request_json.get('view_status')
    service_type_id = request_json.get('service_type_id')

    try:
        query = service_type.update(service_name=service_name, service_name_code=service_name_code, service_description=service_description,
                               service_days=service_days,
                                       transportation_mode=transportation_mode,view_status=view_status).where(service_type.service_type_id == service_type_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_service_type_bp.route('/service_type_api/api_update_status_service_type', methods=['POST'])
def api_update_status_service_type():
    result = 0
    request_json = request.get_json()

    service_type_id = request_json.get('service_type_id')
    view_status = request_json.get('view_status')

    try:
        query = service_type.update(view_status=view_status).where(service_type.service_type_id == service_type_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string