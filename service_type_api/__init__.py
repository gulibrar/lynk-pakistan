from flask import Flask

from .servicetype_api import system_service_type_bp


app = Flask(__name__)

app.register_blueprint(system_service_type_bp)