from flask import Flask

from .clients_api import system_client_bp


app = Flask(__name__)

app.register_blueprint(system_client_bp)