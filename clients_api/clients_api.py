from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
from BaseModelLynk import *
import hashlib
import  json
from dataSetsLynk import *
import random
import string

from datetime import datetime

system_client_bp= Blueprint("system_cl", __name__)

def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    now = datetime.now()
    dt_string = now.strftime("%H%M%S%Y%m%d")
    return ''.join(random.choice(chars) for _ in range(size))+str(dt_string)



##################################### clients
@system_client_bp.route('/client/api_get_all_clients', methods=['POST'])
def api_get_all_clients():
    result = 0
    data = ""

    dataSet = clients.select()
    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_client_bp.route('/client/api_get_single_client', methods=['POST'])
def api_get_single_client():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    client_id = request_json.get('client_id')

    # post response from API

    dataSet = clients.select().where(clients.client_id == str(client_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string

@system_client_bp.route('/client/api_add_client_information',methods=['POST'])
def api_add_client_information():
    json_string = ""
    result = ""

    request_json = request.get_json()

    name = request_json.get('name')

    view_status = request_json.get('view_status')
    created_by = request_json.get('created_by')
    created_at = request_json.get('created_at')

    # password1 = hashlib.md5()
    # password1.update(password.encode("utf-8"))
    # password = password1.hexdigest()

    try:
        query = clients.insert(name=name, view_status=view_status, created_by=created_by, created_at=created_at,
                               url_client=id_generator())

        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json.loads(json_string)

@system_client_bp.route('/client/api_update_client', methods=['POST'])
def api_update_client():
    result = 0
    data = ""

    request_json = request.get_json()

    name = request_json.get('name')

    view_status = request_json.get('view_status')
    created_by = request_json.get('created_by')
    created_at = request_json.get('created_at')
    client_id = request_json.get('client_id')

    try:
        query = clients.update(name=name, view_status=view_status,created_by=created_by, created_at=created_at).where(clients.client_id == client_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_client_bp.route('/client/api_update_status_client', methods=['POST'])
def api_update_status_client():
    result = 0
    request_json = request.get_json()

    client_id = request_json.get('client_id')
    view_status = request_json.get('view_status')

    try:
        query = clients.update(view_status=view_status).where(clients.client_id == client_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string
