from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
from BaseModelLynk import *
import hashlib
import  json
from dataSetsLynk import *
import random
import string

from datetime import datetime

system_client_branches_bp= Blueprint("system_cl_branches", __name__)

def id_generator(size=6, chars=string.ascii_lowercase + string.digits):
    now = datetime.now()
    dt_string = now.strftime("%H%M%S%Y%m%d")
    return ''.join(random.choice(chars) for _ in range(size))+str(dt_string)


##################################### client_branch

@system_client_branches_bp.route('/client_branch_api/api_get_all_client_branches', methods=['POST'])
def api_get_all_client_branches():
    result = 0
    data = ""

    dataSet = client_branches.select(clients.url_client, clients.name.alias('client_name'), client_branches.url_branch,
                                     client_branches.name.alias('branch_name'),
                                     client_branches.contact, client_branches.address, client_branches.view_status,
                                     client_branch_type.name.alias('branch_type'),
                                     country.country_name.alias('country_name'),
                                     province_state.name.alias('province_state_name'),
                                     city.city_name.alias('city_name')).join(clients, on=(
                clients.client_id == client_branches.client_id)).join(client_branch_type, on=(
                client_branch_type.branch_type_id == client_branches.branch_type_id)).join(country, on=(
                country.country_id == client_branches.country_id)).join(province_state, on=(
                province_state.province_state_id == client_branches.province_id)).join(city, on=(
                city.city_id == client_branches.city_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_client_branches_bp.route('/client_branch_api/api_get_single_client_branch', methods=['POST'])
def api_get_single_client_branch():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    client_branch_id = request_json.get('client_branch_id')

    # post response from API

    dataSet = client_branches.select().where(client_branches.client_branch_id == str(client_branch_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_client_branches_bp.route('/client_branch_api/api_add_client_branch', methods=['POST'])
def api_add_client_branch():
    json_string = ""
    result=""

    request_json = request.get_json()

    name = request_json.get('name')
    contact=request_json.get('contact')
    longitude="00"
    latitude="00"
    address=request_json.get('address')
    client_id=request_json.get('client_id')
    type_id=request_json.get('type_id')
    view_status=request_json.get('view_status')
    created_by=request_json.get('created_by')

    created_at=request_json.get('created_at')
    country_id=request_json.get('country_id')
    province_id=request_json.get('province_id')
    city_id=request_json.get('city_id')
    try:
        query = client_branches.insert(name=name, contact=contact, longitude=longitude, latitude=latitude,address=address, client_id=client_id, branch_type_id=type_id, view_status=view_status,created_by=created_by,
                                created_at=created_at,url_branch=id_generator(),country_id=country_id,province_id=province_id,city_id=city_id)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except Exception as e:
        print(e)
        result = 0
        json_string = json.dumps({"result": result})

    return json_string



@system_client_branches_bp.route('/client_branch_api/api_update_client_branch', methods=['POST'])
def api_update_client_branch():
    result = 0
    data = ""

    request_json = request.get_json()

    name = request_json.get('name')
    contact = request_json.get('contact')
    longitude = request_json.get('longitude')
    latitude = request_json.get('latitude')
    address = request_json.get('address')
    client_id = request_json.get('client_id')
    type_id = request_json.get('type_id')
    view_status = request_json.get('view_status')
    created_by = request_json.get('created_by')

    created_at = request_json.get('created_at')
    country_id = request_json.get('country_id')
    province_id = request_json.get('province_id')
    city_id = request_json.get('city_id')
    client_branch_id = request_json.get('client_branch_id')

    try:
        query = client_branches.update(name=name, contact=contact, longitude=longitude, latitude=latitude,address=address, client_id=client_id, branch_type_id=type_id, view_status=view_status,created_by=created_by,
                                created_at=created_at,country_id=country_id,province_id=province_id,city_id=city_id).where(
            client_branches.client_branch_id == client_branch_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_client_branches_bp.route('/client_branch_api/api_update_status_client_branch', methods=['POST'])
def api_update_status_client_branch():
    result = 0
    request_json = request.get_json()

    client_branch_id = request_json.get('client_branch_id')
    view_status = request_json.get('view_status')

    try:
        query = client_branches.update(view_status=view_status).where(
            client_branches.client_branch_id == client_branch_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_client_branches_bp.route('/client_branch_api/api_parent_child_name', methods=['POST'])
def api_parent_child_name():
    result = 0
    data_parent = ""
    data_child = ""
    request_json = request.get_json()

    parent = request_json.get('parent')
    child = request_json.get('child')

    try:
        dataSet = clients.select().where(clients.url_client == parent)
        temp_data = list(dataSet.dicts())
        data_parent = temp_data

        dataSetChild = client_branches.select().where(client_branches.url_branch == child)
        temp_dataChild = list(dataSetChild.dicts())
        data_child = temp_dataChild

        result = 1

    except Exception as e:
        result = e
        print(e)

    json_string = json.dumps({"result": str(result), "data_parent": data_parent, "data_child": data_child})
    return json_string

##################################### client_branch_type
@system_client_branches_bp.route('/client_branch_api/api_get_all_client_branch_type', methods=['POST'])
def api_get_all_client_branch_type():
    result = 0
    data = ""

    dataSet = client_branch_type.select().where(client_branch_type.view_status == 1)

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string