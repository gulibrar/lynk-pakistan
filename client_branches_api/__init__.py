from flask import Flask

from .client_branches_api import system_client_branches_bp


app = Flask(__name__)

app.register_blueprint(system_client_branches_bp)