from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_vehicle_bp= Blueprint("system_vehicle", __name__)

##################################### vehicle
@system_vehicle_bp.route('/vehicle_api/api_get_all_vehicle', methods=['POST'])
def api_get_all_vehicle():
    result = 0
    data = ""

    dataSet = vehicle.select()
    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_vehicle_bp.route('/vehicle_api/api_get_single_vehicle', methods=['POST'])
def api_get_single_vehicle():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    vehicle_id = request_json.get('vehicle_id')

    # post response from API

    dataSet = vehicle.select().where(vehicle.vehicle_id == str(vehicle_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_vehicle_bp.route('/vehicle_api/api_add_new_vehicle', methods=['POST'])
def api_add_new_vehicle():
    json_string = ""
    result = ""

    request_json = request.get_json()

    name= request_json.get('name')
    number=request_json.get('number')
    chasis_number=request_json.get('chasis_number')
    view_status = request_json.get('view_status')

    try:
        query = vehicle.insert(name=name,number=number,chasis_number=chasis_number,view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string


@system_vehicle_bp.route('/vehicle_api/api_update_vehicle', methods=['POST'])
def api_update_vehicle():
    result = 0
    data = ""

    request_json = request.get_json()

    name = request_json.get('name')
    number = request_json.get('number')
    chasis_number = request_json.get('chasis_number')
    view_status = request_json.get('view_status')
    vehicle_id = request_json.get('vehicle_id')


    try:
        query = vehicle.update(name=name,number=number,chasis_number=chasis_number,view_status=view_status).where(vehicle.vehicle_id == vehicle_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_vehicle_bp.route('/vehicle_api/api_update_status_vehicle', methods=['POST'])
def api_update_status_vehicle():
    result = 0
    request_json = request.get_json()

    vehicle_id = request_json.get('vehicle_id')
    view_status = request_json.get('view_status')

    try:
        query = vehicle.update(view_status=view_status).where(vehicle.vehicle_id == vehicle_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string