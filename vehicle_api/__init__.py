from flask import Flask

from .vehicle_api import system_vehicle_bp


app = Flask(__name__)

app.register_blueprint(system_vehicle_bp)