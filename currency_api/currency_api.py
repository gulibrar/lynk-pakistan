from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_currency_bp= Blueprint("system_curr", __name__)

#################################### currency
@system_currency_bp.route('/currency/api_get_all_currency',methods=['POST'])
def api_get_all_currency():
    result = 0
    data = ""

    dataSet = currency.select()
    temp_data = list(dataSet.dicts())
    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string

@system_currency_bp.route('/currency/api_get_single_currency',methods=['POST'])
def api_get_single_currency():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    currency_id = request_json.get('currency_id')


    # post response from API

    dataSet = currency.select().where(currency.currency_id == str(currency_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_currency_bp.route('/currency/api_add_currency', methods=['POST'])
def api_add_currency():
    json_string = ""
    result = ""

    request_json = request.get_json()

    country_id = request_json.get('country_id')
    currency_name = request_json.get('currency_name')
    currency_code = request_json.get('currency_code')
    currency_symbol = request_json.get('currency_symbol')
    view_status = request_json.get('view_status')

    try:
        query = currency.insert(country_id=country_id, currency_name=currency_name,
                                    currency_code=currency_code,
                                    currency_symbol=currency_symbol,view_status=view_status)

        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})
    return json_string


@system_currency_bp.route('/currency/api_update_currency', methods=['POST'])
def api_update_currency():
    result = 0
    data = ""

    request_json = request.get_json()

    country_id = request_json.get('country_id')
    currency_name = request_json.get('currency_name')
    currency_code = request_json.get('currency_code')
    currency_symbol = request_json.get('currency_symbol')
    view_status = request_json.get('view_status')
    currency_id = request_json.get('currency_id')

    try:
        query = currency.update(country_id=country_id, currency_name=currency_name,
                                    currency_code=currency_code,
                                    currency_symbol=currency_symbol,view_status=view_status).where(currency.currency_id == currency_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_currency_bp.route('/currency/api_update_status_currency', methods=['POST'])
def api_update_status_currency():
    result = 0
    request_json = request.get_json()

    currency_id = request_json.get('currency_id')
    view_status = request_json.get('view_status')

    try:
        query = currency.update(view_status=view_status).where(currency.currency_id == currency_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_currency_bp.route('/currency/api_price_conversion',methods=['POST'])
def api_price_conversion():
    result=""
    json_string = ""
    request_json = request.get_json()

    userProfileCurrencyID = str(request_json.get('currency_id_to'))
    currencyID =str(request_json.get('currency_id_from'))
    totalPrice = str(request_json.get('totalPrice'))
    print(totalPrice)

    if(currencyID != userProfileCurrencyID):
        dataSet = (currency_conversion
                   .select((currency_conversion.currency_id_from_rate * currency_conversion.currency_id_to_rate * totalPrice).alias('convertedPrice'), currency.currency_code)
                   .join(currency, on=(currency_conversion.currency_id_to == currency.currency_id))
                   .where((currency_conversion.currency_id_from == currencyID) &
                          (currency_conversion.currency_id_to == userProfileCurrencyID)))




        temp_data = list(dataSet.dicts())
        print(temp_data)

        row_num = len(temp_data)
        if row_num == 0:
            result = 0
            json_string = json.dumps({"result": result})
        else:
            result = 1
            data = temp_data
        # for insert and update query add cursor.commit()
            json_string = json.dumps({"result": result, "data": data})
        return json_string

# else:

    # dataSet = (currency.select((totalPrice),currency.currency_code).where(currency.currency_id==userProfileCurrencyID))
    # print(dataSet)
    # temp_data = list(dataSet.dicts())
    # print(temp_data)
    #
    # row_num = len(temp_data)
    # if row_num == 0:
    #     result = 0
    #     json_string = json.dumps({"result": result})
    # else:
    #     result = 1
    #     data = temp_data
    # # for insert and update query add cursor.commit()
    #     json_string = json.dumps({"result": result, "data": data})
    # return json_string