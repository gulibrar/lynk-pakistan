from flask import Flask

from .currency_api import system_currency_bp


app = Flask(__name__)

app.register_blueprint(system_currency_bp)