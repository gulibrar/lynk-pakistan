from flask import Flask

from .employee_api import system_employee_bp


app = Flask(__name__)

app.register_blueprint(system_employee_bp)