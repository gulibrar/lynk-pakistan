from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_employee_bp= Blueprint("system_employee", __name__)

@system_employee_bp.route('/employee_api/api_login_client_employee', methods=['POST'])
def api_login_client_employee():
    result = 0
    data = ""

    request_json = request.get_json()
    email = request_json.get('email')
    client_id = request_json.get('parent_id')
    branch_id = request_json.get('child_id')

    password_form = request_json.get('password')
    password = hashlib.md5()
    password.update(password_form.encode("utf-8"))
    encyp_pass = password.hexdigest()

    dataSet = employee.select().where(
        (employee.password == str(encyp_pass)) & (employee.employee_email == str(email)) & (
                    employee.view_status == 1) & (employee.client_id == client_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        employee_id = temp_data[0]['employee_id']

        dataSetBranchCheck = client_branch_employees.select().where(
            (client_branch_employees.client_id == client_id) & (client_branch_employees.branch_id == str(branch_id)) & (
                        client_branch_employees.view_status == 1) & (
                        client_branch_employees.employee_id == employee_id))
        temp_dataBC = list(dataSetBranchCheck.dicts())
        if len(temp_dataBC) == 0:
            result = 2
            data = ""
        else:
            result = 1
            data = temp_data

    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})

    return json_string

##################################### employee
@system_employee_bp.route('/employee_api/api_get_all_employee',methods=['POST'])
def api_get_all_employee():

    result=0
    data=""

    dataSet = employee.select(employee.employee_id,employee.first_name,employee.last_name,employee.employee_email,employee.employee_address,employee.view_status,designation.designation_name)\
        .join(designation, on=(designation.designation_id == employee.designation_id)).join(country, on=(designation.designation_id == employee.designation_id))

    temp_data = list(dataSet.dicts())

    row_num=len(temp_data)
    if row_num==0:
        result=0
        data=""
    else:
        result = 1
        data=temp_data


    json_string = json.dumps({"result": result,"data":data})

    return json_string

@system_employee_bp.route('/employee_api/api_get_single_employee',methods=['POST'])
def api_get_single_employee():

    result=0
    data=""

    # post response from API
    request_json = request.get_json()
    employee_id = request_json.get('employee_id')

    # post response from API

    dataSet = employee.select().where(employee.employee_id == str(employee_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result,"data":data})
    return json_string

@system_employee_bp.route('/employee_api/api_add_new_employee',methods=['POST'])
def api_add_new_employee():
    json_string = ""
    result=""

    request_json = request.get_json()

    first_name=request_json.get('first_name')
    last_name=request_json.get('last_name')
    user_name=request_json.get('user_name')
    #password=request_json.get('password')
    employee_type_id=request_json.get('employee_type_id')
    employee_email=request_json.get('employee_email')
    employee_contact_mobile=request_json.get('employee_contact_mobile')
    employee_address=request_json.get('employee_address')
    employee_dob=request_json.get('employee_dob')
    designation_id=request_json.get('designation_id')
    client_id=request_json.get('client_id')
    CNIC=request_json('CNIC')
    country_id=request_json('country_id')
    province_id=request_json('province_id')
    city_id=request_json('city_id')
    view_status=request_json.get('view_status')

    password_form = "abc123"
    password = hashlib.md5()
    password.update(password_form.encode("utf-8"))
    encyp_pass = password.hexdigest()


    try:
        query = employee.insert(first_name=first_name, last_name=last_name, user_name=user_name, password=encyp_pass,
                                       employee_type_id=employee_type_id, employee_email=employee_email,employee_contact_mobile=employee_contact_mobile,
                                       employee_address=employee_address,client_id=client_id, designation_id=designation_id,
                                CNIC=CNIC,country_id=country_id,province_id=province_id,city_id=city_id,employee_dob=employee_dob, view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string

@system_employee_bp.route('/employee_api/api_update_employee',methods=['POST'])
def api_update_employee():
    result=0
    data=""

    request_json = request.get_json()
    first_name=request_json.get('first_name')
    last_name=request_json.get('last_name')
    user_name=request_json.get('user_name')
    password=request_json.get('password')
    employee_type_id=request_json.get('employee_type_id')
    employee_email=request_json.get('employee_email')
    employee_contact_mobile=request_json.get('employee_contact_mobile')
    employee_address=request_json.get('employee_address')
    employee_dob=request_json.get('employee_dob')
    designation_id=request_json.get('designation_id')
    client_id=request_json.get('client_id')
    CNIC = request_json('CNIC')
    country_id = request_json('country_id')
    province_id = request_json('province_id')
    city_id = request_json('city_id')
    view_status=request_json.get('view_status')
    employee_id = request_json.get('employee_id')


    try:
        query = employee.update(first_name=first_name, last_name=last_name, user_name=user_name, password=password,
                                employee_type_id=employee_type_id, employee_email=employee_email,
                                employee_contact_mobile=employee_contact_mobile,
                                employee_address=employee_address, client_id=client_id, designation_id=designation_id,
                                CNIC=CNIC,country_id=country_id,province_id=province_id,city_id=city_id,employee_dob=employee_dob, view_status=view_status).where(employee.employee_id == employee_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0


    json_string = json.dumps({"result": result})
    return json_string

@system_employee_bp.route('/employee_api/api_update_status_employee',methods=['POST'])
def api_update_status_employee():
    result=0
    request_json = request.get_json()
    employee_id = request_json.get('employee_id')
    view_status=request_json.get('view_status')

    try:
        query = employee.update(view_status=view_status).where(employee.employee_id == employee_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string

################################## client_branch_employee

@system_employee_bp.route('/employee_api/api_addBulk_client_branch_employee',methods=['POST'])
def api_addBulk_client_branch_employee():
    json_string = ""
    result = ""

    request_json = request.get_json()

    data=request_json.get('data')
    print(data)
    try:
        client_branch_employees.insert_many(data).execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string

@system_employee_bp.route('/employee_api/api_add_client_branch_employee',methods=['POST'])
def api_add_client_branch_employee():
    json_string = ""
    result = ""

    request_json = request.get_json()

    client_id = request_json.get('client_id')
    branch_id=request_json.get('branch_id')
    employee_id=request_json.get('employee_id')
    view_status=request_json.get('view_status')

    try:
        query = client_branch_employees.insert(client_id=client_id, branch_id=branch_id, employee_id=employee_id,view_status=view_status)


        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})
    return json_string

@system_employee_bp.route('/employee_api/api_update_client_branch_employee', methods=['POST'])
def api_update_client_branch_employee():
    result = 0
    data = ""

    request_json = request.get_json()

    client_id = request_json.get('client_id')
    branch_id = request_json.get('branch_id')
    employee_id = request_json.get('employee_id')
    view_status = request_json.get('view_status')


    try:
        if(view_status==0):
            query = client_branch_employees.update(view_status=1).where( client_branch_employees.employee_id == employee_id)
            query.execute()

        else:
            query = client_branch_employees.update(view_status=0).where(client_branch_employees.employee_id == employee_id)
            query.execute()


        query = client_branch_employees.insert(client_id=client_id, branch_id=branch_id, employee_id=employee_id,
                                               view_status=view_status)
        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string

@system_employee_bp.route('/employee_api/api_update_status_client_branch_employee', methods=['POST'])
def api_update_status_client_branch_employee():
    result = 0
    request_json = request.get_json()

    client_branch_emp_id = request_json.get('client_branch_emp_id')
    view_status = request_json.get('view_status')

    try:
        query = client_branch_employees.update(view_status=view_status).where(client_branch_employees.client_branch_emp_id == client_branch_emp_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string