from flask import Flask
from flask_cors import CORS, cross_origin

from admin_api import system_admin_api_bp
from clients_api import system_client_bp
from currency_api import system_currency_bp
from city_api import system_city_bp
from vehicle_api import system_vehicle_bp
from route_api import system_route_bp
from designation_api import system_designation_bp
from country_api import system_country_bp
from  employee_api import system_employee_bp
from service_type_api import system_service_type_bp
from client_branches_api import system_client_branches_bp
from province_api import system_province_bp



app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.register_blueprint(system_admin_api_bp)
app.register_blueprint(system_client_bp)
app.register_blueprint(system_currency_bp)
app.register_blueprint(system_city_bp)
app.register_blueprint(system_vehicle_bp)
app.register_blueprint(system_route_bp)
app.register_blueprint(system_designation_bp)
app.register_blueprint(system_country_bp)
app.register_blueprint(system_employee_bp)
app.register_blueprint(system_service_type_bp)
app.register_blueprint(system_client_branches_bp)
app.register_blueprint(system_province_bp)




app.secret_key = b'_5#y2L"F4    Q8z\n\xec]/'

if __name__ == '__main__':
    app.run(port='8080')