from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_city_bp= Blueprint("system_city", __name__)

##################################### city
@system_city_bp.route('/city_api/api_get_all_city',methods=['POST'])
def api_get_all_city():

    result=0
    data=""

    dataSet = city.select(city.city_id,
                          city.city_name,
                          country.country_name.alias('country_name'),
                          province_state.name.alias('province_state_name'),
                          city.city_code,
                          city.view_status,
                          city.city_dialing_code).join(country, on=(country.country_id == city.country_id)).join(province_state, on=(province_state.province_state_id == city.state_province_id))
    temp_data = list(dataSet.dicts())

    row_num=len(temp_data)
    if row_num==0:
        result=0
        data=""
    else:
        result = 1
        data=temp_data


    json_string = json.dumps({"result": result,"data":data})

    return json_string

@system_city_bp.route('/city_api/api_get_single_city',methods=['POST'])
def api_get_single_city():

    result=0
    data=""

    # post response from API
    request_json = request.get_json()
    city_id = request_json.get('city_id')

    # post response from API

    dataSet = city.select(city.city_id,
                          city.city_name,
                          country.country_name.alias('country_name'),
                          province_state.name.alias('province_state_name'),
                          city.city_code,
                          city.view_status,
                          city.city_dialing_code).join(country, on=(country.country_id == city.country_id)).join(province_state, on=(province_state.province_state_id == city.state_province_id)).where(city.city_id == str(city_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result,"data":data})
    return json_string

@system_city_bp.route('/city_api/api_add_new_city',methods=['POST'])
def api_add_new_city():
    json_string = ""
    result=""

    request_json = request.get_json()

    city_name=request_json.get('city_name')
    city_code=request_json.get('city_code')
    country_id=request_json.get('country_id')
    state_province_id=request_json.get('state_province_id')
    city_dialing_code = request_json.get('city_dialing_code')
    view_status=request_json.get('view_status')

    try:
        query = city.insert(city_name=city_name,city_code=city_code,country_id=country_id,state_province_id=state_province_id,city_dialing_code=city_dialing_code,view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string

@system_city_bp.route('/city_api/api_update_city',methods=['POST'])
def api_update_city():
    result=0
    data=""

    request_json = request.get_json()
    city_name = request_json.get('city_name')
    city_code = request_json.get('city_code')
    country_id = request_json.get('country_id')
    state_province_id = request_json.get('state_province_id')
    city_dialing_code = request_json.get('city_dialing_code')
    view_status = request_json.get('view_status')
    city_id = request_json.get('city_id')

    try:
        query = city.update(city_name=city_name,city_code=city_code,country_id=country_id,state_province_id=state_province_id,city_dialing_code=city_dialing_code,view_status=view_status).where(city.city_id == city_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0


    json_string = json.dumps({"result": result})
    return json_string

@system_city_bp.route('/city_api/api_update_status_city',methods=['POST'])
def api_update_status_city():
    result=0
    request_json = request.get_json()
    city_id = request_json.get('city_id')
    view_status=request_json.get('view_status')

    try:
        query = city.update(view_status=view_status).where(city.city_id == city_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string

@system_city_bp.route('/city_api/view_city_of_province',methods=['POST'])
def view_city_of_province():

    result=0
    data=""

    request_json = request.get_json()
    country_id           = request_json.get('country_id')
    province_id           = request_json.get('province_id')

    dataSet = city.select(city.city_id,city.city_name).where(city.country_id==country_id,city.state_province_id == province_id, city.view_status==1)
    temp_data = list(dataSet.dicts())
    row_num=len(temp_data)
    if row_num==0:
        result  =0
        data    =""
    else:
        result  = 1
        data    =temp_data

    json_string = json.dumps({"result": result,"data":data})

    return json.loads(json_string)