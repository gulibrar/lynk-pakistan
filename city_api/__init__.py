from flask import Flask

from .city_api import system_city_bp


app = Flask(__name__)

app.register_blueprint(system_city_bp)