from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_country_bp= Blueprint("system_country", __name__)

##################################### country
@system_country_bp.route('/country_api/api_get_all_country', methods=['POST'])
def api_get_all_country():
    result = 0
    data = ""

    dataSet = country.select()
    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_country_bp.route('/country_api/api_get_single_country', methods=['POST'])
def api_get_single_country():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    country_id = request_json.get('country_id')

    # post response from API

    dataSet = country.select().where(country.country_id == str(country_id))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_country_bp.route('/country_api/api_add_new_country', methods=['POST'])
def api_add_new_country():
    json_string = ""
    result = ""

    request_json = request.get_json()

    country_name = request_json.get('country_name')
    country_code = request_json.get('country_code')


    view_status = request_json.get('view_status')

    try:
        query = country.insert(country_name=country_name, country_code=country_code,view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string


@system_country_bp.route('/country_api/api_update_country', methods=['POST'])
def api_update_country():
    result = 0
    data = ""

    request_json = request.get_json()

    country_name = request_json.get('country_name')
    country_code = request_json.get('country_code')
    view_status = request_json.get('view_status')
    country_id = request_json.get('country_id')


    try:
        query = country.update(country_name=country_name, country_code=country_code,view_status=view_status).where(country.country_id == country_id)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_country_bp.route('/country_api/api_update_status_country', methods=['POST'])
def api_update_status_country():
    result = 0
    request_json = request.get_json()
    country_id = request_json.get('country_id')
    view_status = request_json.get('view_status')

    try:
        query = country.update(view_status=view_status).where(country.country_id == country_id)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string