from flask import Flask

from .country_api import system_country_bp


app = Flask(__name__)

app.register_blueprint(system_country_bp)