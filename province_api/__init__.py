from flask import Flask

from .province_api import system_province_bp


app = Flask(__name__)

app.register_blueprint(system_province_bp)