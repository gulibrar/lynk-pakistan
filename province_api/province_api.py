from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
from BaseModelLynk import *
import hashlib
import  json
from dataSetsLynk import *
import random
import string

from datetime import datetime

system_province_bp= Blueprint("system_province", __name__)

@system_province_bp.route('/province_api/api_add_province_state',methods=['POST'])
def add_province_state():
    json_string = ""
    result = ""

    request_json = request.get_json()
    name           = request_json.get('name')
    code           = request_json.get('code')
    country_id           = request_json.get('country_id')
    view_status    = request_json.get('view_status')

    try:
        query = province_state.insert(name=name, code=code,country_id=country_id,
                                view_status=view_status)


        query.execute()


        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json.loads(json_string)

@system_province_bp.route('/province_api/view_province_state',methods=['POST'])
def view_province_state():


    result=0
    data=""

    dataSet = province_state.select(province_state.province_state_id,
                                    province_state.country_id,
                                    province_state.name,
                                    country.country_name.alias('country_name'),
                                    province_state.code,
                                    province_state.view_status).join(country, on=(country.country_id == province_state.country_id))
    temp_data = list(dataSet.dicts())
    row_num=len(temp_data)
    if row_num==0:
        result  =0
        data    =""
    else:
        result  = 1
        data    =temp_data

    json_string = json.dumps({"result": result,"data":data})

    return json.loads(json_string)

@system_province_bp.route('/province_api/view_province_of_country',methods=['POST'])
def view_province_of_country():


    result=0
    data=""
    request_json = request.get_json()
    country_id           = request_json.get('country_id')

    dataSet = province_state.select(province_state.province_state_id,province_state.name).where(province_state.country_id == country_id, province_state.view_status==1)
    temp_data = list(dataSet.dicts())
    row_num=len(temp_data)
    if row_num==0:
        result  =0
        data    =""
    else:
        result  = 1
        data    =temp_data

    json_string = json.dumps({"result": result,"data":data})

    return json.loads(json_string)