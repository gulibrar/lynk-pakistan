from flask import Flask

from .route_api import system_route_bp


app = Flask(__name__)

app.register_blueprint(system_route_bp)