from flask import Flask, render_template, Response, jsonify, json, request, session, redirect, url_for, flash,Blueprint
import hashlib
import  json
from dataSetsLynk import *
from BaseModelLynk import *


system_route_bp= Blueprint("system_route", __name__)

##################################### route
@system_route_bp.route('/route_api/api_get_all_routes', methods=['POST'])
def api_get_all_routes():
    result = 0
    data = ""

    dataSet = tbl_route.select(tbl_route.route_name,city.city_name,country.country_name,province_state.name.alias("province"),tbl_route.description,tbl_route.view_status).join(city, on=(city.city_id == tbl_route.cityID)).join(country, on=(country.country_id == tbl_route.country_id)).join(province_state, on=(province_state.province_state_id== tbl_route.state_province_id))
    print(dataSet)
    temp_data = list(dataSet.dicts())
    print(temp_data)

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string


@system_route_bp.route('/route_api/api_get_single_route', methods=['POST'])
def api_get_single_route():
    result = 0
    data = ""

    # post response from API
    request_json = request.get_json()
    routeID = request_json.get('routeID')

    # post response from API

    dataSet = tbl_route.select().where(tbl_route.routeID == str(routeID))

    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data
    # for insert and update query add cursor.commit()
    json_string = json.dumps({"result": result, "data": data})
    return json_string


@system_route_bp.route('/route_api/api_add_new_route', methods=['POST'])
def api_add_new_route():
    json_string = ""
    result = ""

    request_json = request.get_json()

    route_name= request_json.get('route_name')
    latitude=request_json.get('latitude')
    longitude=request_json.get('longitude')
    cityID=request_json.get('cityID')
    state_province_id=request_json('state_province_id')
    country_id=request_json('country_id')
    description=request_json('description')
    view_status = request_json.get('view_status')

    try:
        query = tbl_route.insert(route_name=route_name,latitude=latitude,longitude=longitude,cityID=cityID,state_province_id=state_province_id,
                                 country_id=country_id,description=description,view_status=view_status)
        query.execute()
        result = 1
        json_string = json.dumps({"result": result})
    except:
        result = 0
        json_string = json.dumps({"result": result})

    return json_string


@system_route_bp.route('/route_api/api_update_route', methods=['POST'])
def api_update_route():
    result = 0
    data = ""

    request_json = request.get_json()

    route_name = request_json.get('route_name')
    latitude = request_json.get('latitude')
    longitude = request_json.get('longitude')
    cityID = request_json.get('cityID')
    state_province_id = request_json('state_province_id')
    country_id = request_json('country_id')
    description = request_json('description')
    view_status = request_json.get('view_status')
    routeID = request_json.get('routeID')


    try:
        query = tbl_route.update(route_name=route_name,latitude=latitude,longitude=longitude,cityID=cityID,state_province_id=state_province_id,
                                 country_id=country_id,description=description,view_status=view_status).where(tbl_route.routeID == routeID)

        query.execute()

        result = 1
    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string


@system_route_bp.route('/route_api/api_update_status_route', methods=['POST'])
def api_update_status_route():
    result = 0
    request_json = request.get_json()

    routeID = request_json.get('routeID')
    view_status = request_json.get('view_status')

    try:
        query = tbl_route.update(view_status=view_status).where(tbl_route.routeID == routeID)
        query.execute()

        result = 1

    except Exception as e:
        result = 0

    json_string = json.dumps({"result": result})
    return json_string

##################################### vehcile - route
@system_route_bp.route('/route_api/api_get_all_vehcile_routes', methods=['POST'])
def api_get_all_vehcile_routes():
    result = 0
    data = ""

    dataSet = vehcile_route.select(vehicle.name,tbl_route.route_name,vehcile_route.view_status).join(vehicle, on=(vehicle.vehicle_id == vehcile_route.vehcile_id)).join(tbl_route, on=(tbl_route.routeID == vehcile_route.route_id))
    temp_data = list(dataSet.dicts())

    row_num = len(temp_data)
    if row_num == 0:
        result = 0
        data = ""
    else:
        result = 1
        data = temp_data

    json_string = json.dumps({"result": result, "data": data})

    return json_string