import peewee as pw
import datetime
from BaseModelLynk import BaseModel


class city(BaseModel):
    city_id = pw.AutoField()
    city_name = pw.TextField()
    city_code = pw.TextField()
    country_id = pw.IntegerField()
    state_province_id = pw.IntegerField()
    view_status = pw.IntegerField()
    city_dialing_code = pw.TextField()

    class Meta:
        table_name = 'city'


class clients(BaseModel):
    client_id = pw.AutoField()
    name = pw.TextField()
    poc_name = pw.TextField()
    poc_phone = pw.TextField()
    head_branch_address = pw.TextField()
    user_name = pw.TextField()
    password = pw.TextField()
    created_by = pw.IntegerField()
    created_at = pw.TextField()
    updated_by = pw.IntegerField()
    updated_at = pw.TextField()
    view_status = pw.IntegerField()
    url_client = pw.TextField()

    class Meta:
        table_name = 'clients'


class client_branch_type(BaseModel):
    branch_type_id = pw.AutoField()
    name = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'client_branch_type'


class client_branches(BaseModel):
    client_branch_id = pw.AutoField()
    name = pw.TextField()
    contact = pw.TextField()
    latitude = pw.TextField()
    longitude = pw.TextField()
    address = pw.TextField()
    client_id = pw.IntegerField()
    branch_type_id = pw.IntegerField()
    view_status = pw.IntegerField()
    created_by = pw.IntegerField()
    created_at = pw.TextField()
    updated_by = pw.IntegerField()
    updated_at = pw.TextField()
    url_branch = pw.TextField()
    country_id = pw.IntegerField()
    province_id = pw.IntegerField()
    city_id = pw.IntegerField()

    class Meta:
        table_name = 'client_branches'


class client_branch_employees(BaseModel):
    client_branch_emp_id = pw.AutoField()
    client_id = pw.IntegerField()
    branch_id = pw.IntegerField()
    employee_id = pw.IntegerField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'client_branch_employees'


class country(BaseModel):
    country_id = pw.AutoField()
    country_name = pw.TextField()
    country_code = pw.TextField()
    country_dialing_code = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'country'


class currency(BaseModel):
    currency_id = pw.AutoField()
    country_id = pw.IntegerField()
    currency_name = pw.TextField()
    currency_symbol = pw.TextField()
    currency_code = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'currency'


class currency_conversion(BaseModel):
    currency_con_id = pw.AutoField()
    currency_id_from = pw.IntegerField()
    currency_id_to = pw.IntegerField()
    currency_id_from_rate = pw.FloatField()
    currency_id_to_rate = pw.FloatField()
    valid_from = pw.DateField()
    valid_till = pw.DateField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'currency_conversion'


class designation(BaseModel):
    designation_id = pw.AutoField()
    designation_name = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'designation'


class employee(BaseModel):
    employee_id = pw.AutoField()
    first_name = pw.TextField()
    last_name = pw.TextField()
    user_name = pw.TextField()
    password = pw.TextField()
    employee_type_id = pw.IntegerField()
    employee_email = pw.TextField()
    employee_contact_mobile = pw.TextField()
    employee_address = pw.TextField()
    employee_dob = pw.TextField()
    designation_id = pw.IntegerField()
    client_id = pw.IntegerField()
    CNIC=pw.TextField()
    county_id=pw.IntegerField()
    province_id=pw.IntegerField()
    city_id=pw.IntegerField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'employee'


class tbl_route(BaseModel):
    routeID = pw.AutoField()
    route_name = pw.TextField()
    latitude = pw.TextField()
    longitude = pw.TextField()
    cityID = pw.IntegerField()
    state_province_id = pw.IntegerField()
    country_id = pw.IntegerField()
    description = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'tbl_route'


class vehcile_route(BaseModel):
    vehcile_route_id = pw.AutoField()
    vehcile_id = pw.IntegerField()
    route_id = pw.IntegerField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'vehcile_route'


class service_type(BaseModel):
    service_type_id = pw.AutoField()
    service_name = pw.TextField()
    service_name_code = pw.TextField()
    service_description = pw.TextField()
    service_days = pw.IntegerField()
    transportation_mode = pw.IntegerField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'service_type'


class statuses(BaseModel):
    status_id = pw.AutoField()
    description = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'statuses'


class vehicle(BaseModel):
    vehicle_id = pw.AutoField()
    name = pw.TextField()
    number = pw.TextField()
    chasis_number = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'vehicle'


class lynk_admin(BaseModel):
    id = pw.AutoField()
    email = pw.TextField()
    passowrd = pw.TextField()

    class Meta:
        table_name = 'lynk_admin'


class province_state(BaseModel):
    province_state_id = pw.AutoField()
    country_id = pw.IntegerField()
    name = pw.TextField()
    code = pw.TextField()
    view_status = pw.IntegerField()

    class Meta:
        table_name = 'province_state'